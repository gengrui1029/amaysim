package net.ruigeng.amaysim.login;

import android.support.test.runner.AndroidJUnit4;

import net.ruigeng.amaysim.data.source.AccountDataSource;
import net.ruigeng.amaysim.data.source.local.AccountLocalDataSource;
import net.ruigeng.amaysim.login.models.Account;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by rui.geng on 12/2/17.
 */
@RunWith(AndroidJUnit4.class)
public class Login {

    private AccountLocalDataSource mLocalDataSource;

    @Before
    public void setup() {
        mLocalDataSource = AccountLocalDataSource.getInstance();
    }

    @Test
    public void loginMsn_checkMsn() {
        final String msn = "0468874507";

        mLocalDataSource.login(msn, new AccountDataSource.LoginCallback() {
            @Override
            public void onUsernameMatches(Account account) {
                assertThat(account.getServices().getMsn(), is(msn));
            }

            @Override
            public void onUsernameError() {
                fail("Callback error");
            }
        });
    }
}
