package net.ruigeng.amaysim.login.domain.usecase;

import android.support.annotation.NonNull;

import net.ruigeng.amaysim.UseCase;
import net.ruigeng.amaysim.data.source.AccountDataSource;
import net.ruigeng.amaysim.data.source.AccountRepository;
import net.ruigeng.amaysim.login.models.Account;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by rui.geng on 11/2/17.
 */

public class UserLogin extends UseCase<UserLogin.RequestValues, UserLogin.ResponseValue> {

    private final AccountRepository mAccountRepository;

    public UserLogin(@NonNull AccountRepository accountRepository) {
        mAccountRepository = checkNotNull(accountRepository, "accountRepository cannot be null!");
    }

    @Override
    protected void executeUseCase(RequestValues requestValues) {
        String username = requestValues.getUsername();
        mAccountRepository.login(username, new AccountDataSource.LoginCallback() {
            @Override
            public void onUsernameMatches(Account account) {
                getUseCaseCallback().onSuccess(new ResponseValue());
            }

            @Override
            public void onUsernameError() {
                getUseCaseCallback().onError();
            }
        });
    }


    public static final class RequestValues implements UseCase.RequestValues {

        private final String mUserLogin;

        public RequestValues(@NonNull String userLogin) {
            mUserLogin = checkNotNull(userLogin, "username cannot be NULL!");
        }

        String getUsername() {
            return mUserLogin;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {
    }
}
