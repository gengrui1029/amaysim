package net.ruigeng.amaysim.login.models;


import com.squareup.moshi.Json;

import java.util.List;

import moe.banana.jsonapi2.HasMany;
import moe.banana.jsonapi2.JsonApi;
import moe.banana.jsonapi2.Resource;

/**
 * Created by rui.geng on 9/2/17.
 */
@JsonApi(type = "services")
public class Service extends Resource {

    @Json(name = "msn")
    private String msn;

    @Json(name = "credit")
    private double credit;

    @Json(name = "credit-expiry")
    private String creditExpiry;

    private HasMany<Subscription> subscriptions;

    public String getMsn() {
        return msn;
    }

    public void setMsn(String msn) {
        this.msn = msn;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public String getCreditExpiry() {
        return creditExpiry;
    }

    public void setCreditExpiry(String creditExpiry) {
        this.creditExpiry = creditExpiry;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions.get(getContext());
    }
}
