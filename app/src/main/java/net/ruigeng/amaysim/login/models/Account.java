package net.ruigeng.amaysim.login.models;


import com.squareup.moshi.Json;

import moe.banana.jsonapi2.HasOne;
import moe.banana.jsonapi2.JsonApi;
import moe.banana.jsonapi2.Resource;

/**
 * Created by rui.geng on 9/2/17.
 */

@JsonApi(type = "accounts", priority = 1)
public class Account extends Resource {

    @Json(name = "payment-type")
    private String paymentType;

    @Json(name = "title")
    private String title;

    @Json(name = "first-name")
    private String firstName;

    @Json(name = "last-name")
    private String lastName;

    @Json(name = "email-address")
    private String emailAddress;

    @Json(name = "email-address-veryfied")
    private boolean emailVerified;

    private HasOne<Service> services;

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Service getServices() {
        return services.get(getContext());
    }
}
