package net.ruigeng.amaysim.login.models;

import com.squareup.moshi.Json;

import moe.banana.jsonapi2.JsonApi;
import moe.banana.jsonapi2.Resource;

/**
 * Created by rui.geng on 9/2/17.
 */

@JsonApi(type = "products")
public class Product extends Resource {

    @Json(name = "name")
    private String name;

    @Json(name = "price")
    private double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
