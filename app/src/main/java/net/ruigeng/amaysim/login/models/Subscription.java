package net.ruigeng.amaysim.login.models;

import com.squareup.moshi.Json;

import moe.banana.jsonapi2.HasOne;
import moe.banana.jsonapi2.JsonApi;
import moe.banana.jsonapi2.Resource;

/**
 * Created by rui.geng on 9/2/17.
 */
@JsonApi(type = "subscriptions")
public class Subscription extends Resource {

    @Json(name = "included-data-balance")
    private double includeDateBalance;

    @Json(name = "expiry-date")
    private String expiryDate;

    @Json(name = "auto-renewal")
    private boolean autoRenewal;

    @Json(name = "primary-subscription")
    private boolean primarySubscription;

    public HasOne<Product> products;

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean isAutoRenewal() {
        return autoRenewal;
    }

    public void setAutoRenewal(boolean autoRenewal) {
        this.autoRenewal = autoRenewal;
    }

    public boolean isPrimarySubscription() {
        return primarySubscription;
    }

    public void setPrimarySubscription(boolean primarySubscription) {
        this.primarySubscription = primarySubscription;
    }

    public double getIncludeDateBalance() {
        return includeDateBalance;
    }

    public void setIncludeDateBalance(double includeDateBalance) {
        this.includeDateBalance = includeDateBalance;
    }

    public Product getProduct() {
        return products.get(getContext());
    }
}
