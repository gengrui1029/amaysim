package net.ruigeng.amaysim.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.jakewharton.rxbinding.widget.RxTextView;

import net.ruigeng.amaysim.R;
import net.ruigeng.amaysim.info.InfoActivity;
import net.ruigeng.amaysim.login.utils.Validator;
import net.ruigeng.amaysim.util.ActivityUtils;
import net.ruigeng.amaysim.util.ScreenUtils;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by rui.geng on 11/2/17.
 */

public class LoginFragment extends Fragment implements LoginContract.View {

    private CompositeSubscription mCompositeSubscription = new CompositeSubscription();

    private TextInputLayout mLayoutMsn;
    private EditText mTxtMsn;
    private Button mBtnSignIn;

    private LoginContract.Presenter mPresenter;

    public static LoginFragment getInstance() {
        return new LoginFragment();
    }

    public LoginFragment() {
    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        mLayoutMsn = (TextInputLayout) rootView.findViewById(R.id.til_msn);

        mTxtMsn = (EditText) rootView.findViewById(R.id.et_msn);

        mBtnSignIn = (Button) rootView.findViewById(R.id.btn_sign_in);
        mBtnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Hide the keyboard when the SignIn button is clicked
                ScreenUtils.hideKeyboard(getActivity());

                // Delay the login process to simulate the network latency.
                mTxtMsn.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Process the login
                        mPresenter.userLogin(mTxtMsn.getText().toString());
                    }
                }, 200);
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Observable<CharSequence> msnChangeObservable = RxTextView.textChanges(mTxtMsn);

        Subscription msnSubscription = msnChangeObservable
                .doOnNext(new Action1<CharSequence>() {
                    @Override
                    public void call(CharSequence charSequence) {
                        hideMsnError();
                        disableSignIn();
                    }
                })
                .debounce(100, TimeUnit.MILLISECONDS)
                .filter(new Func1<CharSequence, Boolean>() {
                    @Override
                    public Boolean call(CharSequence charSequence) {
                        return !TextUtils.isEmpty(charSequence);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CharSequence>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(CharSequence charSequence) {
                        boolean isMsnValid = Validator.isMsnValid(charSequence);
                        if (!isMsnValid) {
                            showMsnError();
                            disableSignIn();
                        } else {
                            hideMsnError();
                            enableSignIn();
                        }
                    }
                });

        mCompositeSubscription.add(msnSubscription);

        mTxtMsn.requestFocus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCompositeSubscription.unsubscribe();
    }

    @Override
    public void showLoginError() {
        Snackbar.make(getActivity().findViewById(android.R.id.content),
                R.string.sign_in_error_cannot_find_msn,
                Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void moveToInfo() {
        Snackbar.make(getActivity().findViewById(android.R.id.content),
                getString(R.string.sign_in_success),
                Snackbar.LENGTH_SHORT)
                .show();

        if (getView() != null) {
            getView().getHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ActivityUtils.startActivity(getActivity(), InfoActivity.class);
                }
            }, 2000);
        }
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    /**
     * Set the error message when the msn input is not valid
     */
    private void showMsnError() {
        enableError(mLayoutMsn);
        mLayoutMsn.setError(getString(R.string.msn_error_msg));
    }

    /**
     * Hide the error message when the msn input is valid
     */
    private void hideMsnError() {
        disableError(mLayoutMsn);
        mLayoutMsn.setError(null);
    }

    /**
     * Show the error message on the msn layout
     *
     * @param textInputLayout the msn layout
     */
    private void enableError(TextInputLayout textInputLayout) {
        if (textInputLayout.getChildCount() == 2)
            textInputLayout.getChildAt(1).setVisibility(View.VISIBLE);
    }

    /**
     * Hide the error message on the msn layout
     *
     * @param textInputLayout the msn layout
     */
    private void disableError(TextInputLayout textInputLayout) {
        if (textInputLayout.getChildCount() == 2)
            textInputLayout.getChildAt(1).setVisibility(View.GONE);
    }

    /**
     * Enable the SignIn button
     */
    private void enableSignIn() {
        mBtnSignIn.setEnabled(true);
        mBtnSignIn.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_white_1000));
    }

    /**
     * Disable the SignIn button
     */
    private void disableSignIn() {
        mBtnSignIn.setEnabled(false);
        mBtnSignIn.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_500));
    }
}
