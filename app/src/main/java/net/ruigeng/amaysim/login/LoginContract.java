package net.ruigeng.amaysim.login;

import net.ruigeng.amaysim.BasePresenter;
import net.ruigeng.amaysim.BaseView;

/**
 * Created by rui.geng on 11/2/17.
 */

public class LoginContract {

    interface View extends BaseView<Presenter> {
        /**
         * Show login error when the msn is not matched.
         */
        void showLoginError();

        /**
         * Start the info screen when the msn is matched.
         */
        void moveToInfo();

        /**
         * Check if the view is added
         * @return  TRUE if it is, or else FALSE
         */
        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        /**
         * Login with the given msn.
         *
         * @param msn the input msn
         */
        void userLogin(String msn);
    }
}
