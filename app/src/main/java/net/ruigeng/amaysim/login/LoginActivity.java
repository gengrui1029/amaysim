package net.ruigeng.amaysim.login;

import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.AppCompatActivity;

import net.ruigeng.amaysim.R;
import net.ruigeng.amaysim.data.Injection;
import net.ruigeng.amaysim.util.ActivityUtils;
import net.ruigeng.amaysim.util.EspressoIdlingResource;

public class LoginActivity extends AppCompatActivity {

    private LoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginFragment loginFragment = (LoginFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (loginFragment == null) {
            loginFragment = LoginFragment.getInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    loginFragment, R.id.contentFrame);
        }

        mLoginPresenter = new LoginPresenter(Injection.provideUseCaseHandler(),
                loginFragment,
                Injection.provideUserLogin(getApplicationContext()));
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }
}
