package net.ruigeng.amaysim.login.utils;

import android.text.TextUtils;

/**
 * Created by rui.geng on 11/2/17.
 */

public class Validator {

    /**
     * Check if the input text is 10 digits
     *
     * @param msn the input text
     * @return TRUE if it is, or else FALSE
     */
    public static boolean isMsnValid(CharSequence msn) {
        return !TextUtils.isEmpty(msn) && msn.length() == 10;
    }
}
