package net.ruigeng.amaysim.login;

import android.support.annotation.NonNull;

import net.ruigeng.amaysim.UseCase;
import net.ruigeng.amaysim.UseCaseHandler;
import net.ruigeng.amaysim.login.domain.usecase.UserLogin;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by rui.geng on 11/2/17.
 */

class LoginPresenter implements LoginContract.Presenter {

    private final LoginContract.View mLoginView;
    private final UserLogin mUserLogin;

    private final UseCaseHandler mUseCaseHandler;

    LoginPresenter(@NonNull UseCaseHandler useCaseHandler,
                   @NonNull LoginContract.View loginView, @NonNull UserLogin userLogin) {
        mUseCaseHandler = checkNotNull(useCaseHandler, "useCaseHandler cannot be null");
        mLoginView = checkNotNull(loginView, "loginView cannot be null");
        mUserLogin = checkNotNull(userLogin, "userLogin cannot be null");

        mLoginView.setPresenter(this);
    }

    @Override
    public void start() {

    }


    @Override
    public void userLogin(String msn) {
        mUseCaseHandler.execute(mUserLogin, new UserLogin.RequestValues(msn),
                new UseCase.UseCaseCallback<UserLogin.ResponseValue>() {
                    @Override
                    public void onSuccess(UserLogin.ResponseValue response) {
                        mLoginView.moveToInfo();
                    }

                    @Override
                    public void onError() {
                        mLoginView.showLoginError();
                    }
                });
    }
}
