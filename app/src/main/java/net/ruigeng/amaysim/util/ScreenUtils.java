package net.ruigeng.amaysim.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by rui.geng on 11/2/17.
 */

public class ScreenUtils {

    /**
     * Hide the keyboard
     *
     * @param activity the context activity
     */
    public static void hideKeyboard(Activity activity) {
        View view = activity.getWindow().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
