package net.ruigeng.amaysim.info;

import net.ruigeng.amaysim.BasePresenter;
import net.ruigeng.amaysim.BaseView;
import net.ruigeng.amaysim.login.models.Account;

/**
 * Created by rui.geng on 11/2/17.
 */

public class InfoContract {

    interface View extends BaseView<Presenter> {

        /**
         * Show Info details
         */
        void showInfoDetails(Account account);

        /**
         * Check if the view is active
         */
        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        /**
         * Get account
         */
        void getAccount();
    }
}
