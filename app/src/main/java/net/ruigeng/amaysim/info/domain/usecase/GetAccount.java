package net.ruigeng.amaysim.info.domain.usecase;

import android.support.annotation.NonNull;

import net.ruigeng.amaysim.UseCase;
import net.ruigeng.amaysim.data.source.AccountDataSource;
import net.ruigeng.amaysim.data.source.AccountRepository;
import net.ruigeng.amaysim.login.models.Account;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by rui.geng on 11/2/17.
 */

public class GetAccount extends UseCase<GetAccount.RequestValues, GetAccount.ResponseValue> {

    private final AccountRepository mAccountRepositery;

    public GetAccount(@NonNull AccountRepository accountRepositery) {
        mAccountRepositery = checkNotNull(accountRepositery, "accountRepository cannot be null!");
    }

    @Override
    protected void executeUseCase(RequestValues requestValues) {
        mAccountRepositery.getAccount(new AccountDataSource.LoadAccountCallback() {
            @Override
            public void onAccountLoaded(Account account) {
                getUseCaseCallback().onSuccess(new ResponseValue(account));
            }

            @Override
            public void onAccountFailed() {
                getUseCaseCallback().onError();
            }
        });
    }

    public static final class RequestValues implements UseCase.RequestValues {

        public RequestValues() {
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final Account mAccount;

        public ResponseValue(Account account) {
            mAccount = account;
        }

        public Account getAccount() {
            return mAccount;
        }
    }
}
