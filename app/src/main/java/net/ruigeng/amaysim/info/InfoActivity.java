package net.ruigeng.amaysim.info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import net.ruigeng.amaysim.R;
import net.ruigeng.amaysim.data.Injection;
import net.ruigeng.amaysim.util.ActivityUtils;

/**
 * Created by rui.geng on 11/2/17.
 */

public class InfoActivity extends AppCompatActivity {

    private InfoPresenter mInfoPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_info);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        InfoFragment infoFragment = (InfoFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (infoFragment == null) {
            infoFragment = InfoFragment.getInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    infoFragment, R.id.contentFrame);
        }

        mInfoPresenter = new InfoPresenter(Injection.provideUseCaseHandler(),
                infoFragment,
                Injection.provideGetAccount(getApplicationContext()));
    }
}
