package net.ruigeng.amaysim.info;

import android.support.annotation.NonNull;

import net.ruigeng.amaysim.UseCase;
import net.ruigeng.amaysim.UseCaseHandler;
import net.ruigeng.amaysim.info.domain.usecase.GetAccount;

/**
 * Created by rui.geng on 11/2/17.
 */

public class InfoPresenter implements InfoContract.Presenter {

    private final InfoContract.View mInfoView;
    private final GetAccount mGetAccount;

    private final UseCaseHandler mUseCaseHandler;

    public InfoPresenter(@NonNull UseCaseHandler useCaseHandler,
                         @NonNull InfoContract.View infoView,
                         @NonNull GetAccount getAccount) {
        mUseCaseHandler = useCaseHandler;
        mGetAccount = getAccount;
        mInfoView = infoView;

        mInfoView.setPresenter(this);
    }

    @Override
    public void getAccount() {

        mUseCaseHandler.execute(mGetAccount, new GetAccount.RequestValues(),
                new UseCase.UseCaseCallback<GetAccount.ResponseValue>() {
                    @Override
                    public void onSuccess(GetAccount.ResponseValue response) {

                        mInfoView.showInfoDetails(response.getAccount());
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    @Override
    public void start() {

    }
}
