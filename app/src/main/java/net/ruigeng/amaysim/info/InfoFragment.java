package net.ruigeng.amaysim.info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.ruigeng.amaysim.R;
import net.ruigeng.amaysim.login.models.Account;
import net.ruigeng.amaysim.login.models.Product;
import net.ruigeng.amaysim.login.models.Service;
import net.ruigeng.amaysim.login.models.Subscription;

/**
 * Created by rui.geng on 11/2/17.
 */

public class InfoFragment extends Fragment implements InfoContract.View {

    // Profile View
    private InfoRowView mViewName;
    private InfoRowView mViewEmail;
    private TextView mTxtEmailNotVerified;

    // Service View
    private InfoRowView mViewServiceCredit;
    private InfoRowView mViewServiceCreditExpiry;

    // Subscription View
    private InfoRowView mViewBalance;
    private InfoRowView mViewAutoRenewal;

    // Product View
    private InfoRowView mViewProductName;
    private InfoRowView mViewProductPrice;

    private InfoContract.Presenter mPresenter;

    public static InfoFragment getInstance() {
        return new InfoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_info, container, false);

        mViewName = (InfoRowView) rootView.findViewById(R.id.view_name);
        mViewEmail = (InfoRowView) rootView.findViewById(R.id.view_email);
        mTxtEmailNotVerified = (TextView) rootView.findViewById(R.id.email_not_verified);
        mViewServiceCredit = (InfoRowView) rootView.findViewById(R.id.view_credit);
        mViewServiceCreditExpiry = (InfoRowView) rootView.findViewById(R.id.view_credit_expired);
        mViewBalance = (InfoRowView) rootView.findViewById(R.id.view_included_data_balance);
        mViewAutoRenewal = (InfoRowView) rootView.findViewById(R.id.view_auto_renewal);
        mViewProductName = (InfoRowView) rootView.findViewById(R.id.view_product_name);
        mViewProductPrice = (InfoRowView) rootView.findViewById(R.id.view_product_price);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPresenter.getAccount();
    }

    @Override
    public void showInfoDetails(Account account) {

        // Profile
        mViewName.setText(R.string.title_name,
                getString(R.string.value_name_format,
                        account.getTitle(),
                        account.getFirstName(),
                        account.getLastName()));

        mTxtEmailNotVerified.setVisibility(account.isEmailVerified() ? View.GONE : View.VISIBLE);
        mViewEmail.setText(R.string.title_email, account.getEmailAddress());

        // Service
        Service service = account.getServices();
        mViewServiceCredit.setText(R.string.title_credit, String.format("%.2f", service.getCredit() / 100));

        mViewServiceCreditExpiry.setText(R.string.title_credit_expired, service.getCreditExpiry());

        // Subscription
        Subscription subscription = service.getSubscriptions().get(0);
        double balance = subscription == null ? 0 : subscription.getIncludeDateBalance();
        mViewBalance.setText(R.string.title_included_data_balance, String.format("%.2f", balance / 1000));

        mViewAutoRenewal.setText(R.string.title_auto_renewal,
                subscription.isAutoRenewal() ? getString(R.string.yes) : getString(R.string.no)
        );

        // Product
        Product product = subscription.getProduct();
        mViewProductName.setText(R.string.title_product_name, product.getName());

        mViewProductPrice.setText(R.string.title_product_price, String.format("%.2f", product.getPrice() / 100));
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(InfoContract.Presenter presenter) {
        mPresenter = presenter;
    }
}
