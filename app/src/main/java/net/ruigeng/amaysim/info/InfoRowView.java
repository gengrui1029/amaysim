package net.ruigeng.amaysim.info;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.ruigeng.amaysim.R;

/**
 * Created by rui.geng on 11/2/17.
 */

public class InfoRowView extends LinearLayout {

    private TextView mTxtName;
    private TextView mTxtValue;

    public InfoRowView(Context context) {
        super(context);
        init();
    }

    public InfoRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public InfoRowView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_info_rows, this, true);

        mTxtName = (TextView) findViewById(R.id.tv_name);
        mTxtValue = (TextView) findViewById(R.id.tv_value);
    }

    /**
     * Setup the texts for the row
     *
     * @param nameRes the string resource
     * @param value   the value in String format
     */
    public void setText(int nameRes, String value) {
        mTxtName.setText(nameRes);
        mTxtValue.setText(value);
    }
}
