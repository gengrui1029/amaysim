package net.ruigeng.amaysim;

import android.app.Application;

/**
 * Created by rui.geng on 11/2/17.
 */

public class AmaysimApplication extends Application {

    private static AmaysimApplication INSTANCE;

    @Override
    public void onCreate() {
        super.onCreate();

        INSTANCE = this;
    }

    public static AmaysimApplication getInstance() {
        return INSTANCE;
    }
}
