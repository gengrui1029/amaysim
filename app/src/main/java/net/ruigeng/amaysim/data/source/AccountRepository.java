package net.ruigeng.amaysim.data.source;

import android.support.annotation.NonNull;

import net.ruigeng.amaysim.login.models.Account;

/**
 * Created by rui.geng on 11/2/17.
 */

public class AccountRepository implements AccountDataSource {

    private static AccountRepository INSTANCE = null;

    private final AccountDataSource mAccountRemoteDataSource;
    private final AccountDataSource mAccountLocalDataSource;

    private Account mAccount;

    private AccountRepository(@NonNull AccountDataSource accountRemoteDataSource,
                              @NonNull AccountDataSource accountLocalDataSource) {
        mAccountLocalDataSource = accountLocalDataSource;
        mAccountRemoteDataSource = accountRemoteDataSource;
    }

    public static AccountRepository getInstance(AccountDataSource accountRemoteDataSource,
                                                AccountDataSource accountLocalDataSource) {

        if (INSTANCE == null) {
            INSTANCE = new AccountRepository(accountRemoteDataSource, accountLocalDataSource);
        }

        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }


    @Override
    public void getAccount(@NonNull LoadAccountCallback callback) {
        mAccountLocalDataSource.getAccount(callback);
    }

    @Override
    public void login(@NonNull String username, @NonNull LoginCallback callback) {
        mAccountLocalDataSource.login(username, callback);
    }
}
