package net.ruigeng.amaysim.data.source.local;

import android.support.annotation.NonNull;

import net.ruigeng.amaysim.data.source.AccountDataSource;
import net.ruigeng.amaysim.data.utils.MoshiUtils;
import net.ruigeng.amaysim.login.models.Account;

/**
 * Created by rui.geng on 11/2/17.
 */

public class AccountLocalDataSource implements AccountDataSource {

    private static AccountLocalDataSource INSTANCE;

    public static AccountLocalDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AccountLocalDataSource();
        }
        return INSTANCE;
    }

    // Cached Account object.
    private Account mAccount;

    @Override
    public void getAccount(@NonNull LoadAccountCallback callback) {
        if (mAccount != null) {
            callback.onAccountLoaded(mAccount);
        } else {
            callback.onAccountFailed();
        }
    }

    @Override
    public void login(@NonNull String username, @NonNull LoginCallback callback) {
        mAccount = MoshiUtils.parseAccount();

        if (mAccount != null && mAccount.getServices().getMsn().equals(username)) {
            callback.onUsernameMatches(mAccount);
        } else {
            callback.onUsernameError();
        }
    }
}
