package net.ruigeng.amaysim.data.source;

import android.support.annotation.NonNull;

import net.ruigeng.amaysim.login.models.Account;

/**
 * Created by rui.geng on 11/2/17.
 */

public interface AccountDataSource {

    interface LoadAccountCallback {

        void onAccountLoaded(Account account);

        void onAccountFailed();
    }

    interface LoginCallback {

        void onUsernameMatches(Account account);

        void onUsernameError();
    }

    void getAccount(@NonNull LoadAccountCallback callback);

    void login(@NonNull String username, @NonNull LoginCallback callback);
}
