package net.ruigeng.amaysim.data.utils;

import android.content.res.Resources;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import net.ruigeng.amaysim.AmaysimApplication;
import net.ruigeng.amaysim.R;
import net.ruigeng.amaysim.login.models.Account;
import net.ruigeng.amaysim.login.models.Product;
import net.ruigeng.amaysim.login.models.Service;
import net.ruigeng.amaysim.login.models.Subscription;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;

import moe.banana.jsonapi2.Document;
import moe.banana.jsonapi2.ResourceAdapterFactory;

/**
 * Created by rui.geng on 11/2/17.
 */

public class MoshiUtils {

    /**
     * Read the JSON file from resourceId and convert it to a String
     *
     * @param resourceId the resourceId
     * @return the String text
     */
    public static String readJson(int resourceId) {
        Resources res = AmaysimApplication.getInstance().getResources();
        InputStream inputStream = res.openRawResource(resourceId);
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes);
            return new String(bytes, "UTF8");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    /**
     * Parse the json to get Account object
     * @return  Account object
     */
    public static Account parseAccount() {
        Account account = null;

        // Load the json file
        String json = MoshiUtils.readJson(R.raw.collection);

        JsonAdapter.Factory jsonApiAdapterFactory = ResourceAdapterFactory.builder()
                .add(Account.class)
                .add(Product.class)
                .add(Service.class)
                .add(Subscription.class)
                .build();

        Moshi moshi = new Moshi.Builder()
                .add(jsonApiAdapterFactory)
                .build();

        Type type = Types.newParameterizedType(Document.class, Account.class, Service.class);
        JsonAdapter<Document<Account>> adapter = moshi.adapter(type);

        // Deserialize the json to Account object and cache it if successful
        Document<Account> accounts = null;
        try {
            if (json != null) accounts = adapter.fromJson(json);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (accounts != null) account = accounts.get();
        }

        return account;
    }

}
