/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ruigeng.amaysim.data;

import android.content.Context;
import android.support.annotation.NonNull;

import net.ruigeng.amaysim.UseCaseHandler;
import net.ruigeng.amaysim.data.data.FakeAccountRemoteDataSource;
import net.ruigeng.amaysim.data.source.AccountRepository;
import net.ruigeng.amaysim.data.source.local.AccountLocalDataSource;
import net.ruigeng.amaysim.info.domain.usecase.GetAccount;
import net.ruigeng.amaysim.login.domain.usecase.UserLogin;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Enables injection of mock implementations for
 * {@link net.ruigeng.amaysim.data.source.AccountDataSource} at compile time. This is useful for testing, since it allows us to use
 * a fake instance of the class to isolate the dependencies and run a test hermetically.
 */
public class Injection {

    public static AccountRepository provideAccountRepository(@NonNull Context context) {
        checkNotNull(context);
        return AccountRepository.getInstance(FakeAccountRemoteDataSource.getInstance(),
                AccountLocalDataSource.getInstance());
    }

    public static UseCaseHandler provideUseCaseHandler() {
        return UseCaseHandler.getInstance();
    }

    public static UserLogin provideUserLogin(@NonNull Context context) {
        return new UserLogin(Injection.provideAccountRepository(context));
    }

    public static GetAccount provideGetAccount(@NonNull Context context) {
        return new GetAccount(Injection.provideAccountRepository(context));
    }
}
