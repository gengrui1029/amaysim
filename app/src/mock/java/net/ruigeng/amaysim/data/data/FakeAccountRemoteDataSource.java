package net.ruigeng.amaysim.data.data;

import android.support.annotation.NonNull;

import net.ruigeng.amaysim.data.source.AccountDataSource;
import net.ruigeng.amaysim.data.utils.MoshiUtils;
import net.ruigeng.amaysim.login.models.Account;

/**
 * Created by rui.geng on 11/2/17.
 */

public class FakeAccountRemoteDataSource implements AccountDataSource {

    private static FakeAccountRemoteDataSource INSTANCE;

    private Account mAccount;

    public static FakeAccountRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FakeAccountRemoteDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void getAccount(@NonNull LoadAccountCallback callback) {

    }

    @Override
    public void login(@NonNull String msn, @NonNull LoginCallback callback) {
        mAccount = MoshiUtils.parseAccount();

        if (mAccount != null && mAccount.getServices().getMsn().equals(msn)) {
            callback.onUsernameMatches(mAccount);
        } else {
            callback.onUsernameError();
        }
    }
}
