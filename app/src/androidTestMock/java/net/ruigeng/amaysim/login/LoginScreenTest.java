package net.ruigeng.amaysim.login;

import android.content.Intent;
import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import net.ruigeng.amaysim.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by rui.geng on 12/2/17.
 */
@RunWith(AndroidJUnit4.class)
public class LoginScreenTest {

    @Rule
    public ActivityTestRule<LoginActivity> mLoginActivityTestRule =
            new ActivityTestRule<>(LoginActivity.class, true, false);

    @Before
    public void startLoginScreen() {
        mLoginActivityTestRule.launchActivity(new Intent());

        Espresso.registerIdlingResources(
                mLoginActivityTestRule.getActivity().getCountingIdlingResource());
    }

    @Test
    public void inputMsnAndClickSignIn_failed() {
        inputMsn("0468874501");
        onView(withId(R.id.btn_sign_in)).perform(click());
    }

//    @Test
//    public void inputMsnAndClickSignIn_success() {
//        inputMsn("0468874507");
//        onView(withId(R.id.btn_sign_in)).perform(click());
//    }

    private void inputMsn(String msn) {
        onView(withId(R.id.et_msn)).perform(typeText(msn), closeSoftKeyboard());
    }
}
