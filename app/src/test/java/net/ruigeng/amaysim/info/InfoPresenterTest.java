package net.ruigeng.amaysim.info;

import net.ruigeng.amaysim.TestUseCaseScheduler;
import net.ruigeng.amaysim.UseCaseHandler;
import net.ruigeng.amaysim.data.source.AccountDataSource;
import net.ruigeng.amaysim.data.source.AccountRepository;
import net.ruigeng.amaysim.info.domain.usecase.GetAccount;
import net.ruigeng.amaysim.login.models.Account;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by rui.geng on 12/2/17.
 */
@RunWith(JUnit4.class)
public class InfoPresenterTest {

    @Mock
    private AccountRepository mAccountRepository;

    @Mock
    private InfoContract.View mInfoView;

    private InfoPresenter mInfoPresenter;
    private static Account ACCOUNT;

    @Captor
    private ArgumentCaptor<AccountDataSource.LoadAccountCallback> mLoadAccountCaptor;

    @Before
    public void setup() {
        // Init
        MockitoAnnotations.initMocks(this);

        mInfoPresenter = givenInfoPresenter();

        ACCOUNT = new Account();
        ACCOUNT.setFirstName("Bill");
        ACCOUNT.setLastName("Gates");

        // If the view is not updated, the presenter will not update.
        when(mInfoView.isActive()).thenReturn(true);
    }

    private InfoPresenter givenInfoPresenter() {
        UseCaseHandler useCaseHandler = new UseCaseHandler(new TestUseCaseScheduler());
        GetAccount getAccount = new GetAccount(mAccountRepository);

        return new InfoPresenter(useCaseHandler, mInfoView, getAccount);
    }

    @After
    public void destroy() {
        ACCOUNT = null;
    }

    @Test
    public void loadAccountAndLoadToInfoView() {
        mInfoPresenter.getAccount();

        verify(mAccountRepository).getAccount(mLoadAccountCaptor.capture());
        mLoadAccountCaptor.getValue().onAccountLoaded(ACCOUNT);

        ArgumentCaptor<Account> showInfoDetailsArgumentCaptor = ArgumentCaptor.forClass(Account.class);
        verify(mInfoView).showInfoDetails(showInfoDetailsArgumentCaptor.capture());
        assertTrue(showInfoDetailsArgumentCaptor.getValue().getFirstName() != null);
        assertTrue(showInfoDetailsArgumentCaptor.getValue().getLastName() != null);
    }
}
